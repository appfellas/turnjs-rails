# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'turnjs-rails/version'

Gem::Specification.new do |gem|
  gem.name          = "turnjs-rails"
  gem.version       = TurnjsRails::VERSION
  gem.authors       = ["Mike Bevz"]
  gem.email         = ["myb@appfellas.co"]
  gem.description   = "Turn.js for Rails"
  gem.summary       = "Make a flip book with HTML5"
  gem.homepage      = "http://www.turnjs.com"

  gem.files         = Dir["{lib,vendor}/**/*"] + ['README.md', 'LICENSE.txt']
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]
  gem.add_dependency "railties", "~> 3.1"
end
